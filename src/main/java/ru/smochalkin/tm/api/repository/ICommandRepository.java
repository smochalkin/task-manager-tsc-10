package ru.smochalkin.tm.api.repository;

import ru.smochalkin.tm.model.Command;

public interface ICommandRepository {

    Command[] getCommands();

}
