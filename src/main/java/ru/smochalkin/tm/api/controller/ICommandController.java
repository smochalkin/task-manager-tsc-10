package ru.smochalkin.tm.api.controller;

public interface ICommandController {

    void showHelp();

    void showArguments();

    void showCommands();

    void showCommandValue(String value);

    void showAbout();

    void showVersion();

    void showErrorCommand();

    void showErrorArgument();

    void showInfo();

    void exit();

}
