package ru.smochalkin.tm.api.service;

import ru.smochalkin.tm.model.Task;

import java.util.List;

public interface ITaskService {

    void create(String name);

    void create(String name, String description);

    void add(Task project);

    void remove(Task project);

    List<Task> findAll();

    void clear();
    
}
