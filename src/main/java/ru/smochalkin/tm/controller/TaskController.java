package ru.smochalkin.tm.controller;

import ru.smochalkin.tm.api.controller.ITaskController;
import ru.smochalkin.tm.api.service.ITaskService;
import ru.smochalkin.tm.model.Task;
import ru.smochalkin.tm.util.TerminalUtil;

import java.util.List;

public class TaskController implements ITaskController {

    private final ITaskService taskService;

    public TaskController(ITaskService taskService) {
        this.taskService = taskService;
    }

    @Override
    public void showTasks() {
        System.out.println("[TASK LIST]");
        List<Task> tasks = taskService.findAll();
        for (Task task : tasks) {
            System.out.println(task);
        }
        System.out.println("[OK]");
    }

    @Override
    public void clearTasks() {
        System.out.println("[CLEAR TASKS]");
        taskService.clear();
        System.out.println("[OK]");
    }

    @Override
    public void createTask() {
        System.out.println("[CREATE TASK]");
        System.out.print("Enter name: ");
        String name = TerminalUtil.nextLine();
        System.out.print("Enter description: ");
        String description = TerminalUtil.nextLine();
        taskService.create(name, description);
        System.out.println("[OK]");
    }

}
